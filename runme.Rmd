---
title: How to Run the ```rmbmix``` Package
output:
  html_document:
    theme: default
---

### Mass-spectrometry Dependencies

- [RMassBank by MaliRemorker](https://github.com/MaliRemorker/RMassBank/tree/master)
- [RChemMass](https://github.com/schymane/RChemMass)

### Installation

One way to install it is to clone the repositories to your local
machine, then use ```load_package``` function defined below to
(re)install them. You can, of course, try to

```{r setup, echo=FALSE, results='hide'}
knitr::opts_chunk$set(list(browser='icecat'))

## Define the package reload/reinstall function.
load_package <- function(name,src=NULL,reload_pkgs=NULL) {

    for (name in c(reload_pkgs,name)) {
        pname <- paste0("package:",name)
        if (pname %in% search()) eval(substitute(detach(pname,unload=T),list(pname=pname)))
    }
    if (!is.null(src)) devtools::install(src, upgrade = "never")

    for (name in rev(c(reload_pkgs,name))) {
        eval(substitute(library(name,list(name=name))))
    }
    invisible(NULL)
}
```

### Change to the project directory
```{r}
proj <- "/home/todor/cabinet/projects/skunkworks/rmbmix_testsets/Entact_Rmbtestdata"
setwd(proj)
```

### Create a local package library, if needed
```{r}
dir.create("pkgs",showWarnings = F)
.libPaths(c("pkgs",.libPaths()))
Sys.setenv("R_LIBS_USER"="pkgs")
```
### Load required packages

The first parameter is the name of the package, the second is the
location of the source code.

```{r}
load_package("rmbmix","~/cabinet/projects/ECI/gitlab/rmbmix")
```


### Prepare your inputs
- compound list in a _RMassBank_ format:
  - it is a CSV, comma-separated file
  - required columns: ***ID***, ***SMILES***, ***CAS***, ***RT***, ***Name***
  - columns ***ID*** and ***SMILES*** must be filled; others can be blank
  - quote all ***SMILES*** and ***Name*** entries
  - Entries in ***ID*** are integer, <9999 and unique
  - Entries in ***RT*** --- when given --- are retention times in
    minutes
  
- [Shinyscreen](https://git-r3lab.uni.lu/eci/shinyscreen) file-table
  - this CSV is the output of the prescreening procedure
  - it should contain ```mode``` column using RMassBank terminology for adducts (e.g. _pH_ for [M+H]+)
- The raw data files (in mzML format)




### Inputs

```{r}
cmpds <- file2tab("./20200303_ENTACT_RP_mix499_compounds.csv")
knitr::kable(head(cmpds), caption = "An example of a compounds list.", floating.environment="sidewaystable")
```

```{r}
ftable <- file2tab("./20200303_ENTACT_RP_mix499_pos_ftable.csv")
knitr::kable(head(ftable),caption = "Example file-table.", floating.environment="sidewaystable")
```

```{r}
fn_data <- list.files(pattern = ".*pos.*mzML")
knitr::kable(data.frame(Files=fn_data), caption = "Data files.")
```

```{r}
fn_rmbstgs <- "~/cabinet/projects/skunkworks/rmbmix_testsets/20190702_MOEPEST_generic.ini"
```

### Adapt the File-table

The file-table, as it is, may be unsuitable for further
processing. For example, it may contain more files than we want
through push through the RMassBank workflow and the paths of the files
in it might be obsolete (for example, if the mzMLs have been moved
after the prescreening).

In this section, we adapt the ftable so that it can be passed on to
the additional steps of the workflow.

#### Reduce file table to only those entries that passed all quality checks.
```{r}
ftable <- ftable[MS1 & MS2 & Alignment & AboveNoise & !is.na(iMS2rt)]
```

#### Filter the file table first for existing data

Throw out the files not found in the ```fn_data``` list.
```{r}
basefn <- basename(fn_data) #Of course, basenames of the files must be
                            #unique, or this approach fails. We rely
                            #here on the common sense of the user not
                            #to name files with different data the
                            #same.

ftable_fn <- unique(ftable$Files)
exist_fn <- ftable_fn[basename(ftable_fn) %in% basefn]
exist_fn

ftable <- ftable[Files %in% exist_fn,] #Select only those files that
                                        #we actually have.




```
#### Rewrite paths if needed

If the mzML files have been moved since the file-table was created, we
need to rewrite ***Files*** column path entries, updating the paths.

```{r}
ft_files <- unique(ftable$Files)
print(ft_files)
```

Since all the mzMLs are in the ```proj``` directory, we can replace
the dirnames of the mzMLs with it.

```{r}
ftable[,("Files"):=.(file.path(proj,basename(Files)))]
```

#### Save the file-table
```{r}
tab2file(tab=ftable,file=file.path(proj,"ftable.csv"))
```

#### We also need to create a file settings table

Each collision energy set contains files that will be merged into a
multi-CE workspace. This file will need an associated RMassBank
settings file. In order to automate the generation of the settings
files, we need to provide the ```ce```, ```ces```, ```mode``` and ```res``` 
entries in a separate table. Below, we create it as ```ce_sets``` 
table.  This table also contains the ```cetag``` column
which defines the prefix used to make the _mzML_ and _ini_ filenames.

```{r}
fls <- ftable[,unique(Files)]
ce_sets <- dtable(Files=fls,
                                ce=c(15L,30L,45L),
                                ces=c(15L,30L,45L),
                                res=17500L,
                                modeSL='HCD',
                                cetag="2020303_ENTACT_RP_mix499_pos_comb")
knitr::kable(ce_sets)
```
### Starting with the RMassBank Process

With the file-table ```ftable``` adapted and the collision sets
settings table ```ce_sets``` defined we can start with the RMassBank
workflow.


### Spectral Workflow
```{r}
res<-NULL
if (!is.null(res$cl)) {parallel::stopCluster(res$cl);res$cl <- NULL}
res <- rmbmix::proj_sp_new(proc=4L,fn_ftable="./ftable.csv",fn_rmb_stgs="~/cabinet/projects/skunkworks/rmbmix_testsets/20190702_MOEPEST_generic.ini",fn_cmpds="~/cabinet/projects/skunkworks/rmbmix_testsets/Entact_Rmbtestdata/20200303_ENTACT_RP_mix499_compounds.csv",ce_sets=ce_sets)
res <- rmbmix::proj_sp_conf(res)
res <- rmbmix::proj_sp_init_cl(res)
res <- rmbmix::proj_sp_readin(res)
res <- rmbmix::proj_sp_comb(res)
res <- rmbmix::proj_sp_recl(res)
res <- rmbmix::proj_sp_done(res)
```
### Record Generation
```{r}
res <- rmbmix::proj_mb_info(res)
```

Sometimes one gets the following warnings.
```{r}
## ...
## 37: In getCompTox(inchikey_split) : EPA web service is currently offline
## 38: In getCtsRecord(inchikey_split) :
##   CTS seems to be currently unavailable or incapable of interpreting your request
## 39: In getCompTox(inchikey_split) : EPA web service is currently offline
## 40: In getCtsRecord(inchikey_split) :
##  CTS seems to be currently unavailable or incapable of interpreting your request
## ...
```

```{r}
res <- rmbmix::proj_mb_rec(res)
```

