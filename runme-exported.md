### Mass-spectrometry Dependencies

-   [RMassBank by
    MaliRemorker](https://github.com/MaliRemorker/RMassBank/tree/master)
-   [RChemMass](https://github.com/schymane/RChemMass)

### Installation

One way to install it is to clone the repositories to your local
machine, then use `load_package` function defined below to (re)install
them. You can, of course, try to

### Change to the project directory

    proj <- "/home/todor/cabinet/projects/skunkworks/rmbmix_testsets/Entact_Rmbtestdata"
    setwd(proj)

### Create a local package library, if needed

    dir.create("pkgs",showWarnings = F)
    .libPaths(c("pkgs",.libPaths()))
    Sys.setenv("R_LIBS_USER"="pkgs")

### Load required packages

The first parameter is the name of the package, the second is the
location of the source code.

    load_package("rmbmix","~/cabinet/projects/ECI/gitlab/rmbmix")

    ##      checking for file ‘/home/todor/cabinet/projects/ECI/gitlab/rmbmix/DESCRIPTION’ ...  ✔  checking for file ‘/home/todor/cabinet/projects/ECI/gitlab/rmbmix/DESCRIPTION’
    ##   ─  preparing ‘rmbmix’:
    ##      checking DESCRIPTION meta-information ...  ✔  checking DESCRIPTION meta-information
    ##   ─  checking for LF line-endings in source and make files and shell scripts
    ##   ─  checking for empty or unneeded directories
    ##   ─  building ‘rmbmix_0.2.tar.gz’
    ##      
    ## Running \
    ##   /gnu/store/wzf59r3ys74zy1x1wb0x0yi6q91afsrb-r-minimal-4.0.0/lib/R/bin/R CMD \
    ##   INSTALL /tmp/RtmpXEjiwC/rmbmix_0.2.tar.gz --install-tests 
    ## -* installing to library ‘/home/todor/cabinet/projects/skunkworks/rmbmix_testsets/Entact_Rmbtestdata/pkgs’
    ## \* installing *source* package ‘rmbmix’ ...
    ## |** using staged installation
    ## /** R
    ## -** byte-compile and prepare package for lazy loading
    ## \|/-\|/-\|/-\|/-\|/-\|/** help
    ## -*** installing help indices
    ## \** building package indices
    ## |/** testing if installed package can be loaded from temporary location
    ## -\|/-\|/-\|/-\|/-\|/-** testing if installed package can be loaded from final location
    ## \|/-\|/-\|/-\|/-\|/-** testing if installed package keeps a record of temporary installation path
    ## \* DONE (rmbmix)
    ## |/ 

### Prepare your inputs

-   compound list in a *RMassBank* format:
    -   it is a CSV, comma-separated file
    -   required columns: ***ID***, ***SMILES***, ***CAS***, ***RT***,
        ***Name***
    -   columns ***ID*** and ***SMILES*** must be filled; others can be
        blank
    -   quote all ***SMILES*** and ***Name*** entries
    -   Entries in ***ID*** are integer, &lt;9999 and unique
    -   Entries in ***RT*** — when given — are retention times in
        minutes
-   [Shinyscreen](https://git-r3lab.uni.lu/eci/shinyscreen) file-table
    -   this CSV is the output of the prescreening procedure
    -   it should contain `mode` column using RMassBank terminology for
        adducts (e.g. *pH* for \[M+H\]+)
-   The raw data files (in mzML format)

### Inputs

    cmpds <- file2tab("./20200303_ENTACT_RP_mix499_compounds.csv")
    knitr::kable(head(cmpds), caption = "An example of a compounds list.", floating.environment="sidewaystable")

<table>
<caption>An example of a compounds list.</caption>
<thead>
<tr class="header">
<th style="text-align: right;">ID</th>
<th style="text-align: left;">Name</th>
<th style="text-align: left;">SMILES</th>
<th style="text-align: left;">CAS</th>
<th style="text-align: left;">RT</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: right;">44</td>
<td style="text-align: left;">Pirimicarb</td>
<td style="text-align: left;">CN(C)C(=O)OC1=C(C)C(C)=NC(=N1)N(C)C</td>
<td style="text-align: left;">23103-98-2</td>
<td style="text-align: left;">NA</td>
</tr>
<tr class="even">
<td style="text-align: right;">55</td>
<td style="text-align: left;">Cybutryne</td>
<td style="text-align: left;">CSC1=NC(NC(C)(C)C)=NC(NC2CC2)=N1</td>
<td style="text-align: left;">28159-98-0</td>
<td style="text-align: left;">NA</td>
</tr>
<tr class="odd">
<td style="text-align: right;">139</td>
<td style="text-align: left;">TDCPP</td>
<td style="text-align: left;">ClCC(CCl)OP(=O)(OC(CCl)CCl)OC(CCl)CCl</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
</tr>
<tr class="even">
<td style="text-align: right;">282</td>
<td style="text-align: left;">Norgestrel</td>
<td style="text-align: left;">CCC12CCC3C(CCC4=CC(=O)CCC34)C1CCC2(O)C#C</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
</tr>
<tr class="odd">
<td style="text-align: right;">289</td>
<td style="text-align: left;">Pioglitazone hydrochloride</td>
<td style="text-align: left;">CCC1=CN=C(CCOC2=CC=C(CC3SC(=O)NC3=O)C=C2)C=C1</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
</tr>
<tr class="even">
<td style="text-align: right;">293</td>
<td style="text-align: left;">Pirinixic acid</td>
<td style="text-align: left;">CC1=C(C)C(NC2=NC(SCC(O)=O)=NC(Cl)=C2)=CC=C1</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
</tr>
</tbody>
</table>

    ftable <- file2tab("./20200303_ENTACT_RP_mix499_pos_ftable.csv")
    knitr::kable(head(ftable),caption = "Example file-table.", floating.environment="sidewaystable")

<table>
<caption>Example file-table.</caption>
<thead>
<tr class="header">
<th style="text-align: left;">Files</th>
<th style="text-align: left;">mode</th>
<th style="text-align: left;">set</th>
<th style="text-align: right;">tag</th>
<th style="text-align: left;">wd</th>
<th style="text-align: right;">ID</th>
<th style="text-align: right;">mz</th>
<th style="text-align: left;">Name</th>
<th style="text-align: right;">rt</th>
<th style="text-align: left;">Comments</th>
<th style="text-align: left;">MS1</th>
<th style="text-align: left;">MS2</th>
<th style="text-align: left;">Alignment</th>
<th style="text-align: left;">AboveNoise</th>
<th style="text-align: right;">MS2rt</th>
<th style="text-align: right;">iMS2rt</th>
<th style="text-align: left;">checked</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML’s/20200303_ENTACT_RP_mix499_pos_CE15.mzML</td>
<td style="text-align: left;">pH</td>
<td style="text-align: left;">mix499_P</td>
<td style="text-align: right;">15</td>
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mix499_P/20200303_ENTACT_RP_mix499_pos_CE15</td>
<td style="text-align: right;">1044</td>
<td style="text-align: right;">142.0418</td>
<td style="text-align: left;">4-Chloro-2-methylaniline hydrochloride</td>
<td style="text-align: right;">13.3563280</td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: left;">FALSE</td>
<td style="text-align: left;">FALSE</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">AUTO</td>
</tr>
<tr class="even">
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML’s/20200303_ENTACT_RP_mix499_pos_CE15.mzML</td>
<td style="text-align: left;">pH</td>
<td style="text-align: left;">mix499_P</td>
<td style="text-align: right;">15</td>
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mix499_P/20200303_ENTACT_RP_mix499_pos_CE15</td>
<td style="text-align: right;">1045</td>
<td style="text-align: right;">167.0637</td>
<td style="text-align: left;">Ethionamide</td>
<td style="text-align: right;">6.6673225</td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: right;">6.606924</td>
<td style="text-align: right;">1</td>
<td style="text-align: left;">AUTO</td>
</tr>
<tr class="odd">
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML’s/20200303_ENTACT_RP_mix499_pos_CE15.mzML</td>
<td style="text-align: left;">pH</td>
<td style="text-align: left;">mix499_P</td>
<td style="text-align: right;">15</td>
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mix499_P/20200303_ENTACT_RP_mix499_pos_CE15</td>
<td style="text-align: right;">1046</td>
<td style="text-align: right;">233.0921</td>
<td style="text-align: left;">Phenobarbital sodium</td>
<td style="text-align: right;">0.3145712</td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">FALSE</td>
<td style="text-align: left;">FALSE</td>
<td style="text-align: left;">FALSE</td>
<td style="text-align: left;">FALSE</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">AUTO</td>
</tr>
<tr class="even">
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML’s/20200303_ENTACT_RP_mix499_pos_CE15.mzML</td>
<td style="text-align: left;">pH</td>
<td style="text-align: left;">mix499_P</td>
<td style="text-align: right;">15</td>
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mix499_P/20200303_ENTACT_RP_mix499_pos_CE15</td>
<td style="text-align: right;">1047</td>
<td style="text-align: right;">399.0758</td>
<td style="text-align: left;">Sulfasalazine</td>
<td style="text-align: right;">16.9266400</td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: right;">16.955644</td>
<td style="text-align: right;">1</td>
<td style="text-align: left;">AUTO</td>
</tr>
<tr class="odd">
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML’s/20200303_ENTACT_RP_mix499_pos_CE15.mzML</td>
<td style="text-align: left;">pH</td>
<td style="text-align: left;">mix499_P</td>
<td style="text-align: right;">15</td>
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mix499_P/20200303_ENTACT_RP_mix499_pos_CE15</td>
<td style="text-align: right;">1048</td>
<td style="text-align: right;">205.0415</td>
<td style="text-align: left;">2-Chloro-4-phenylphenol</td>
<td style="text-align: right;">26.3212180</td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: left;">FALSE</td>
<td style="text-align: left;">FALSE</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">AUTO</td>
</tr>
<tr class="even">
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML’s/20200303_ENTACT_RP_mix499_pos_CE15.mzML</td>
<td style="text-align: left;">pH</td>
<td style="text-align: left;">mix499_P</td>
<td style="text-align: right;">15</td>
<td style="text-align: left;">/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mix499_P/20200303_ENTACT_RP_mix499_pos_CE15</td>
<td style="text-align: right;">1049</td>
<td style="text-align: right;">181.1223</td>
<td style="text-align: left;">Olivetol</td>
<td style="text-align: right;">26.2650650</td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: left;">FALSE</td>
<td style="text-align: left;">TRUE</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">AUTO</td>
</tr>
</tbody>
</table>

    fn_data <- list.files(pattern = ".*pos.*mzML")
    knitr::kable(data.frame(Files=fn_data), caption = "Data files.")

<table>
<caption>Data files.</caption>
<thead>
<tr class="header">
<th style="text-align: left;">Files</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">20200303_ENTACT_RP_mix499_pos_CE15.mzML</td>
</tr>
<tr class="even">
<td style="text-align: left;">20200303_ENTACT_RP_mix499_pos_CE30.mzML</td>
</tr>
<tr class="odd">
<td style="text-align: left;">20200303_ENTACT_RP_mix499_pos_CE45.mzML</td>
</tr>
</tbody>
</table>

    fn_rmbstgs <- "~/cabinet/projects/skunkworks/rmbmix_testsets/20190702_MOEPEST_generic.ini"

### Adapt the File-table

The file-table, as it is, may be unsuitable for further processing. For
example, it may contain more files than we want through push through the
RMassBank workflow and the paths of the files in it might be obsolete
(for example, if the mzMLs have been moved after the prescreening).

In this section, we adapt the ftable so that it can be passed on to the
additional steps of the workflow.

#### Reduce file table to only those entries that passed all quality checks.

    ftable <- ftable[MS1 & MS2 & Alignment & AboveNoise & !is.na(iMS2rt)]

#### Filter the file table first for existing data

Throw out the files not found in the `fn_data` list.

    basefn <- basename(fn_data) #Of course, basenames of the files must be
                                #unique, or this approach fails. We rely
                                #here on the common sense of the user not
                                #to name files with different data the
                                #same.

    ftable_fn <- unique(ftable$Files)
    exist_fn <- ftable_fn[basename(ftable_fn) %in% basefn]
    exist_fn

    ## [1] "/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML's/20200303_ENTACT_RP_mix499_pos_CE15.mzML"
    ## [2] "/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML's/20200303_ENTACT_RP_mix499_pos_CE30.mzML"
    ## [3] "/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML's/20200303_ENTACT_RP_mix499_pos_CE45.mzML"

    ftable <- ftable[Files %in% exist_fn,] #Select only those files that
                                            #we actually have.

#### Rewrite paths if needed

If the mzML files have been moved since the file-table was created, we
need to rewrite ***Files*** column path entries, updating the paths.

    ft_files <- unique(ftable$Files)
    print(ft_files)

    ## [1] "/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML's/20200303_ENTACT_RP_mix499_pos_CE15.mzML"
    ## [2] "/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML's/20200303_ENTACT_RP_mix499_pos_CE30.mzML"
    ## [3] "/home/eciae/Documents/scratch/20200303_ENTACT_RP_mix499_pos/mzML's/20200303_ENTACT_RP_mix499_pos_CE45.mzML"

Since all the mzMLs are in the `proj` directory, we can replace the
dirnames of the mzMLs with it.

    ftable[,("Files"):=.(file.path(proj,basename(Files)))]

#### Save the file-table

    tab2file(tab=ftable,file=file.path(proj,"ftable.csv"))

#### We also need to create a file settings table

Each collision energy set contains files that will be merged into a
multi-CE workspace. This file will need an associated RMassBank settings
file. In order to automate the generation of the settings files, we need
to provide the `ce`, `ces`, `mode` and `res` entries in a separate
table. Below, we create it as `ce_sets` table. This table also contains
the `cetag` column which defines the prefix used to make the *mzML* and
*ini* filenames.

    fls <- ftable[,unique(Files)]
    ce_sets <- dtable(Files=fls,
                                    ce=c(15L,30L,45L),
                                    ces=c(15L,30L,45L),
                                    res=17500L,
                                    modeSL='HCD',
                                    cetag="2020303_ENTACT_RP_mix499_pos_comb")
    knitr::kable(ce_sets)

<table>
<thead>
<tr class="header">
<th style="text-align: left;">Files</th>
<th style="text-align: right;">ce</th>
<th style="text-align: right;">ces</th>
<th style="text-align: right;">res</th>
<th style="text-align: left;">modeSL</th>
<th style="text-align: left;">cetag</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">/home/todor/cabinet/projects/skunkworks/rmbmix_testsets/Entact_Rmbtestdata/20200303_ENTACT_RP_mix499_pos_CE15.mzML</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">15</td>
<td style="text-align: right;">17500</td>
<td style="text-align: left;">HCD</td>
<td style="text-align: left;">2020303_ENTACT_RP_mix499_pos_comb</td>
</tr>
<tr class="even">
<td style="text-align: left;">/home/todor/cabinet/projects/skunkworks/rmbmix_testsets/Entact_Rmbtestdata/20200303_ENTACT_RP_mix499_pos_CE30.mzML</td>
<td style="text-align: right;">30</td>
<td style="text-align: right;">30</td>
<td style="text-align: right;">17500</td>
<td style="text-align: left;">HCD</td>
<td style="text-align: left;">2020303_ENTACT_RP_mix499_pos_comb</td>
</tr>
<tr class="odd">
<td style="text-align: left;">/home/todor/cabinet/projects/skunkworks/rmbmix_testsets/Entact_Rmbtestdata/20200303_ENTACT_RP_mix499_pos_CE45.mzML</td>
<td style="text-align: right;">45</td>
<td style="text-align: right;">45</td>
<td style="text-align: right;">17500</td>
<td style="text-align: left;">HCD</td>
<td style="text-align: left;">2020303_ENTACT_RP_mix499_pos_comb</td>
</tr>
<tr class="even">
<td style="text-align: left;">### Starting with the RMassBank Process</td>
<td style="text-align: right;"></td>
<td style="text-align: right;"></td>
<td style="text-align: right;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

With the file-table `ftable` adapted and the collision sets settings
table `ce_sets` defined we can start with the RMassBank workflow.

### Spectral Workflow

    res<-NULL
    if (!is.null(res$cl)) {parallel::stopCluster(res$cl);res$cl <- NULL}
    res <- rmbmix::proj_sp_new(proc=4L,fn_ftable="./ftable.csv",fn_rmb_stgs="~/cabinet/projects/skunkworks/rmbmix_testsets/20190702_MOEPEST_generic.ini",fn_cmpds="~/cabinet/projects/skunkworks/rmbmix_testsets/Entact_Rmbtestdata/20200303_ENTACT_RP_mix499_compounds.csv",ce_sets=ce_sets)
    res <- rmbmix::proj_sp_conf(res)

    ## Warning in RMassBank::loadRmbSettings(fn_stgs): Your settings are outdated.
    ## Missing will be replaced by default values.

    ## Warning in updateSettings(o): Your settings are outdated! The following fields
    ## were taken from default values: include_sp_tags

    ## Loaded compoundlist successfully

    res <- rmbmix::proj_sp_init_cl(res)
    res <- rmbmix::proj_sp_readin(res)

    ## ws cols:Files,mode,modeSL,ce,ces,res,cetag,w

    res <- rmbmix::proj_sp_comb(res)
    res <- rmbmix::proj_sp_recl(res)

    ## msmsWorkflow: Step 2. First analysis pre recalibration

    ## msmsWorkflow: Step 3. Aggregate all spectra

    ## msmsWorkflow: Step 4. Recalibrate m/z values in raw spectra

    ## msmsWorkflow: Done.

![](/home/todor/cabinet/projects/ECI/gitlab/rmbmix/runme-exported_files/figure-markdown_strict/unnamed-chunk-14-1.png)

    res <- rmbmix::proj_sp_done(res)

    ## msmsWorkflow: Step 5. Reanalyze recalibrated spectra

    ## msmsWorkflow: Step 6. Aggregate recalibrated results

    ## msmsWorkflow: Step 7. Reanalyze fail peaks for N2 + O

    ## msmsWorkflow: Step 8. Peak multiplicity filtering

    ## msmsWorkflow: Done.

### Record Generation

    res <- rmbmix::proj_mb_info(res)

    ## mbWorkflow: Step 1. Gather info from several databases

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCtsRecord(inchikey_split): CTS seems to be currently unavailable
    ## or incapable of interpreting your request

    ## 44: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 55: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 289: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 293: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 303: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 310: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 331: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 341: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 342: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 358: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 360: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 389: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 393: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 406: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 409: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 411: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 648: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 998: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1045: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1047: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1072: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1077: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1084: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1085: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1087: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1093: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1096: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1098: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1099: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1114: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1118: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1121: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1123: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1136: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1145: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1147: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1152: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1174: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1175: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1181: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1186: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1188: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1190: smiles

    ## Warning in getCompTox(inchikey_split): EPA web service is currently offline

    ## Warning in getCompTox(inchikey_split): CTS seems to be currently unavailable or
    ## incapable of interpreting your request

    ## 1195: smiles

    ## mbWorkflow: Step 2. Export infolist (if required)

    ## The file /home/todor/cabinet/projects/skunkworks/rmbmix_testsets/Entact_Rmbtestdata/2020303_ENTACT_RP_mix499_pos_comb_pH_CE15_CE30_CE45/XX/2020303_ENTACT_RP_mix499_pos_comb_CE15_CE30_CE45_infolist.csv was generated with new compound information. Please check and edit the table, and add it to your infolist folder.

Sometimes one gets the following warnings.

    ## ...
    ## 37: In getCompTox(inchikey_split) : EPA web service is currently offline
    ## 38: In getCtsRecord(inchikey_split) :
    ##   CTS seems to be currently unavailable or incapable of interpreting your request
    ## 39: In getCompTox(inchikey_split) : EPA web service is currently offline
    ## 40: In getCtsRecord(inchikey_split) :
    ##  CTS seems to be currently unavailable or incapable of interpreting your request
    ## ...

    res <- rmbmix::proj_mb_rec(res)

    ## mbWorkflow: Step 1. Gather info from several databases

    ## mbWorkflow: Step 2. Export infolist (if required)

    ## No new data added.

    ## mbWorkflow: Step 3. Data reformatting

    ## mbWorkflow: Step 4. Spectra compilation

    ## Compiling: Pirimicarb

    ## Compiling: Cybutryne

    ## Compiling: Pioglitazone hydrochloride

    ## Compiling: Pirinixic acid

    ## Compiling: 2-[4-(Diethylamino)-2-hydroxybenzoyl]benzoic acid

    ## Compiling: Ingliforib

    ## Compiling: Pyridaben

    ## Compiling: SB281832

    ## Compiling: MK-274

    ## Compiling: Atorvastatin calcium

    ## Compiling: 1,3-Dipropan-2-ylurea

    ## Compiling: N2-(4-Nitrophenyl)-L-glutamine hydrochloride

    ## Compiling: Fabesetron hydrochloride

    ## Compiling: (4-Aminophenyl)arsonic acid

    ## Compiling: Roxithromycin

    ## Compiling: Penconazole

    ## Compiling: Propamocarb hydrochloride

    ## Compiling: Dicyclohexyl phthalate

    ## Compiling: Ethionamide

    ## Compiling: Sulfasalazine

    ## Compiling: 17-Methyltestosterone

    ## Compiling: 5,7-Dimethoxy-2H-chromen-2-one

    ## Compiling: 4-Methoxy-2-methylaniline

    ## Compiling: Amiloride hydrochloride

    ## Compiling: Fenarimol

    ## Compiling: CP-457677

    ## Compiling: Piperine

    ## Compiling: 2-Amino-6-nitrobenzothiazole

    ## Compiling: 4-Methoxy-2-methyl-N-phenylaniline

    ## Compiling: Mepanipyrim

    ## Compiling: Theophylline

    ## Compiling: Famotidine

    ## Compiling: 2-(Ethyl(3-methylphenyl)amino)acetonitrile

    ## Compiling: 1-Naphthol

    ## Compiling: 7-(Dimethylamino)-4-methylcoumarin

    ## Compiling: Isoprocarb

    ## Compiling: Carabersat

    ## Compiling: Acetochlor

    ## Compiling: Flutolanil

    ## Compiling: Secbumeton

    ## Compiling: 3-Methyl-3,4-dihydro-2H-1,4-benzoxazine

    ## Compiling: N,N'-Dicyclohexylthiourea

    ## Compiling: Tebuconazole

    ## Compiling: Dodecylamine hydrochloride

    ## mbWorkflow: [Legacy Step 5. Flattening records] ignored

    ## mbWorkflow: Step 6. Generate molfiles

    ## mbWorkflow: Step 7. Generate subdirs and export

    ## mbWorkflow: Step 8. Create list.tsv

    ## mbWorkflow: Step 1. Gather info from several databases

    ## mbWorkflow: Step 2. Export infolist (if required)

    ## No new data added.

    ## mbWorkflow: Step 3. Data reformatting

    ## mbWorkflow: Step 4. Spectra compilation

    ## Compiling: Pirimicarb

    ## Compiling: Cybutryne

    ## Compiling: Pioglitazone hydrochloride

    ## Compiling: Pirinixic acid

    ## Compiling: 2-[4-(Diethylamino)-2-hydroxybenzoyl]benzoic acid

    ## Compiling: Ingliforib

    ## Compiling: Pyridaben

    ## Compiling: SB281832

    ## Compiling: MK-274

    ## Compiling: Atorvastatin calcium

    ## Compiling: 1,3-Dipropan-2-ylurea

    ## Compiling: N2-(4-Nitrophenyl)-L-glutamine hydrochloride

    ## Compiling: Fabesetron hydrochloride

    ## Compiling: (4-Aminophenyl)arsonic acid

    ## Compiling: Roxithromycin

    ## Compiling: Penconazole

    ## Compiling: Propamocarb hydrochloride

    ## Compiling: Dicyclohexyl phthalate

    ## Compiling: Ethionamide

    ## Compiling: Sulfasalazine

    ## Compiling: 17-Methyltestosterone

    ## Compiling: 5,7-Dimethoxy-2H-chromen-2-one

    ## Compiling: 4-Methoxy-2-methylaniline

    ## Compiling: Amiloride hydrochloride

    ## Compiling: Fenarimol

    ## Compiling: CP-457677

    ## Compiling: Piperine

    ## Compiling: 2-Amino-6-nitrobenzothiazole

    ## Compiling: 4-Methoxy-2-methyl-N-phenylaniline

    ## Compiling: Mepanipyrim

    ## Compiling: Theophylline

    ## Compiling: Famotidine

    ## Compiling: 2-(Ethyl(3-methylphenyl)amino)acetonitrile

    ## Compiling: 1-Naphthol

    ## Compiling: 7-(Dimethylamino)-4-methylcoumarin

    ## Compiling: Isoprocarb

    ## Compiling: Carabersat

    ## Compiling: Acetochlor

    ## Compiling: Flutolanil

    ## Compiling: Secbumeton

    ## Compiling: 3-Methyl-3,4-dihydro-2H-1,4-benzoxazine

    ## Compiling: N,N'-Dicyclohexylthiourea

    ## Compiling: Tebuconazole

    ## Compiling: Dodecylamine hydrochloride

    ## mbWorkflow: [Legacy Step 5. Flattening records] ignored

    ## mbWorkflow: Step 6. Generate molfiles

    ## mbWorkflow: Step 7. Generate subdirs and export

    ## mbWorkflow: Step 8. Create list.tsv
