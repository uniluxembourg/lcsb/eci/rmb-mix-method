% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/run.R
\name{proj_sp_init_cl}
\alias{proj_sp_init_cl}
\title{Cluster Creation}
\usage{
proj_sp_init_cl(res)
}
\arguments{
\item{res}{Parameter list.}
}
\value{
Parameter list.
}
\description{
Creates a cluster.
}
\author{
Todor Kondić
}
